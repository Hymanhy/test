﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="16008000">
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;7R=2MR%!81N=?"5X&lt;A91M&lt;/W-,&lt;'&amp;&lt;9+K1,7Q,&lt;)%N&lt;!NMA3X)DW?-RJ(JQ"I\%%Z,(@`BA#==ZB3RN;]28_,V7@P_W`:R`&gt;HV*SU_WE@\N_XF[3:^^TX\+2YP)D7K6;G-RV3P)R`ZS%=_]J'XP/5N&lt;XH,7V\SEJ?]Z#5P?=J4HP+5JTTFWS%0?=B$DD1G(R/.1==!IT.+D)`B':\B'2Z@9XC':XC':XBUC?%:HO%:HO&amp;R7QT0]!T0]!S0I4&lt;*&lt;)?=:XA-(]X40-X40-VDSGC?"GC4N9(&lt;)"D2,L;4ZGG?ZH%;T&gt;-]T&gt;-]T?.S.%`T.%`T.)^&lt;NF8J4@-YZ$S'C?)JHO)JHO)R&gt;"20]220]230[;*YCK=ASI2F=)1I.Z5/Z5PR&amp;)^@54T&amp;5TT&amp;5TQO&lt;5_INJ6Z;"[(H#&gt;ZEC&gt;ZEC&gt;Z$"(*ETT*ETT*9^B)HO2*HO2*(F.&amp;]C20]C2)GN4UE1:,.[:/+5A?0^NOS?UJ^3&lt;*\9B9GT@7JISVW7*NIFC&lt;)^:$D`5Q9TWE7)M@;V&amp;D,6;M29DVR]6#R],%GC47T9_/=@&gt;Z5V&gt;V57&gt;V5E&gt;V5(OV?^T[FTP?\`?YX7ZRP6\D=LH%_8S/U_E5R_-R$I&gt;$\0@\W/VW&lt;[_"&lt;Y[X&amp;],0^^+,]T_J&gt;`J@_B_]'_.T`$KO.@I"O[^NF!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">369131520</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Item Name="OPC1" Type="IO Server">
		<Property Name="atrb" Type="Str">(!)!!!Q1!!!,!!!!"B!!!!A!!!"E!'5!91"E!')!91"O!'1!!Q!!!!!!!!!!!!!!"B!!!!M!!!"F!'Y!91"C!'Q!:1"%!'5!9A"V!'=!!A!!!!!'%!!!$1!!!'E!&lt;A"U!'5!=A"G!'%!9Q"F!&amp;1!?1"Q!'5!"B!!!!Q!!!""!(-!?1"O!'-!;!"S!']!&lt;A"P!(5!=Q!'%!!!"Q!!!'Q!&lt;Q"H!%9!;1"M!'5!"B!!!!!!!!!'%!!!"Q!!!'U!91"D!'A!;1"O!'5!"B!!!!E!!!"M!']!9Q"B!'Q!;!"P!(-!&gt;!!'%!!!#Q!!!'U!91"Y!%9!;1"M!'5!5Q"J!(I!:1!$!!9!!!!!!!!!R%!'%!!!"A!!!&amp;!!=A"P!'=!31"%!!91!!!5!!!!5Q!X!$)!-!!Q!&amp;-!41""!&amp;)!6!!O!%]!5!"$!&amp;-!:1"S!(9!:1"S!!91!!!2!!!!=A"F!'-!&lt;Q"O!'Y!:1"D!(1!5!"P!'Q!&lt;!"3!'%!&gt;!"F!!-!!!!!!!!!!!"?1!91!!!3!!!!=Q"F!()!&gt;A"F!()!31"O!(-!&gt;!"B!'Y!9Q"F!&amp;1!?1"Q!'5!"B!!!!M!!!"0!(5!&gt;!!A!']!:A!A!&amp;!!=A"P!'-!"B!!!!I!!!"V!(!!:!"B!(1!:1"3!'%!&gt;!"F!!-!!!!!!!!!!%#01!91!!!.!!!!&gt;1"T!'5!6!"B!'=!:Q"F!()!6!"J!'U!:1!#!!!!!!</Property>
		<Property Name="className" Type="Str">OPC</Property>
	</Item>
	<Item Name="风扇" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\Untitled Library 1.lvlib\OPC1\MWSMART\NewPLC\风扇</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!:&amp;1!!!"9!A!!!!!!"!!1!)1!"!!!"!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\Untitled Library 1.lvlib\OPC1</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">MWSMART\NewPLC\风扇</Property>
	</Item>
	<Item Name="急停" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\Untitled Library 1.lvlib\OPC1\MWSMART\NewPLC\急停</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!:&amp;1!!!"9!A!!!!!!"!!1!)1!"!!!"!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\Untitled Library 1.lvlib\OPC1\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">MWSMART\NewPLC\急停</Property>
	</Item>
</Library>
